var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var PORT = 3000;

app.use(bodyParser.json());

app.get('/', function(req, res) {
    res.send('Hello World: ' + PORT);
});

process.argv.forEach(function(arg) {
    if (/port=[^ ]/.test(arg)) {
        PORT = arg.split('=')[1];
    }
});

app.listen(PORT, function(err) {
    if (!err)
        console.log('Server Started on Port: #' + PORT);
});